package license

type PingResponse struct {
	Message      string `xml:"message"`
	ResponseCode string `xml:"responseCode"`
	Salt         string `xml:"salt"`
}

type ObtainTicketResponse struct {
	Message            string `xml:"message"`
	ProlongationPeriod int    `xml:"prolongationPeriod"`
	ResponseCode       string `xml:"responseCode"`
	Salt               string `xml:"salt"`
	TicketID           int    `xml:"ticketId"`
	TicketProperties   string `xml:"ticketProperties"`
}

func joinTicketProperties(properties map[string]string) string {
	payload := ""
	for name, value := range properties {
		payload += name + "=" + value + "\t"
	}
	return payload
}
